﻿#include <iostream>
#include <iomanip>

class A
{
public:
	A(int x, int y)
	{
		x1 = x;
		y1 = y;

		dynam_array = new int* [x];   // создаем 
		for (int i = 0; i < x; i++) {          // двумерный
			dynam_array[i] = new int[y]; // массив 
		}                                      // !

		for (int count_row = 0; count_row < x; count_row++) {
			std::cout << "New Row";
			for (int count_column = 0; count_column < y; count_column++)
				//dynam_array[count_row][count_column] = (rand() % 10 + 1) / float((rand() % 10 + 1)); //заполнение массива случайными числами с масштабированием от 1 до 10
				std::cin >> dynam_array[count_row][count_column];
		}

		for (int count_row = 0; count_row < x; count_row++)
		{
			for (int count_column = 0; count_column < y; count_column++)
				std::cout << std::setw(4) << std::setprecision(2) << dynam_array[count_row][count_column] << "   ";
			std::cout << std::endl;
		}

		str = new std::string("Important!");
	}

	A(const A& other)
	{
		std::cout << "Copy Constructor\n";
		if (other.b)
		{
			if (b) delete b;
			b = new float(*(other.b));
		}
		if (other.str)
		{
			if (str) delete str;
			str = new std::string(*(other.str));
		}
		if (other.dynam_array)
		{
			if (dynam_array) delete dynam_array;
			x1 = other.x1;
			y1 = other.y1;

			dynam_array = new int* [x1];

			for (int i = 0; i < x1; i++)
				dynam_array[i] = new int[y1];

			for (int i = 0; i < x1; i++)
			{
				for (int j = 0; j < y1; j++)
				{
					dynam_array[i][j] = other.dynam_array[i][j];
				}
			}
		}
	}

	void PrintMass()
	{
		for (int count_row = 0; count_row < x1; count_row++)
		{
			for (int count_column = 0; count_column < y1; count_column++)
				std::cout << std::setw(4) << std::setprecision(2) << dynam_array[count_row][count_column] << "   ";
			std::cout << std::endl;
		}
	}
private:
	int** dynam_array;
	float* b;
	std::string* str;
	int x1, y1;
};

int main()
{
	A i1(2, 4);
	A i2 = i1;
	i2.PrintMass();
}